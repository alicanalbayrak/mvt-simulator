#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
Created on Mar 1, 2014

@author: alican
'''

class Coordinate():
    def __init__(self,Lat,Long):
        self.__Latitude = Lat
        self.__Longtitude = Long

    def get_latitude(self):
        return self.__Latitude


    def get_longtitude(self):
        return self.__Longtitude


    def set_latitude(self, value):
        self.__Latitude = value


    def set_longtitude(self, value):
        self.__Longtitude = value

        
    def get_Coordinate(self):
        coordinate = [self.Latitude, self.Longtitude]
        return coordinate
    
        
    Latitude = property(get_latitude, set_latitude, None, None)
    Longtitude = property(get_longtitude, set_longtitude, None, None)
