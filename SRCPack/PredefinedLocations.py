#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
Created on Mar 15, 2014

@author: alican
'''
from Coordinate import Coordinate

ank_cor = Coordinate(39.8750, 32.8333)
ist_cor = Coordinate(41.0136, 28.9550)
ant_cor = Coordinate(36.9000, 30.6833)
tobb_cor = Coordinate(39.9219, 32.7992)
odtu_ikizler = Coordinate(39.5347,32.4632)
kizilay = Coordinate(39.9167,32.8500)
tunali_hilmi = Coordinate(39.906389,32.860833)
baskent_uni = Coordinate(39.8875,32.6539)
odtu = Coordinate(39.8914,32.7847)
hacettepe = Coordinate(39.8672,32.7344)

definedCoordinates = {  'Ankara' : ank_cor, 
                        'Istanbul' : ist_cor, 
                        'Antalya' : ant_cor,
                        'Tobb Etu': tobb_cor,
                        'Teknokent' : odtu_ikizler,
                        'Kizilay': kizilay,
                        'Tunali Hilmi' : tunali_hilmi,
                        'Baskent Uni': baskent_uni,
                        'Odtu': odtu,
                        'Hacettepe': hacettepe,
                      }