#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
Created on Feb 28, 2014

@author: alican
'''

from PyQt4 import QtCore
import os


def store_file_content(fileName, content):
    """Save content on disk with the given file name."""
    if fileName == '':
        raise Exception()    
    try:
        f = QtCore.QFile(fileName)
        if not f.open(QtCore.QIODevice.WriteOnly | QtCore.QIODevice.Text | QtCore.QIODevice.Truncate):
            raise Exception.message("Dosya hatasi olustu")
        stream = QtCore.QTextStream(f)
        encoding = "UTF-8"
        if encoding:
            stream.setCodec(encoding)
        f.write(content)
        f.flush()
        f.close()
    except:
        raise
    return os.path.abspath(fileName)

