#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
Created on Mar 1, 2014

@author: alican
'''
import json
import math
import FileManager
import datetime
import random
from time import sleep,time
from PathLoader import PathLoader
from Asset import Asset
from Coordinate import Coordinate
from PyQt4 import QtCore


class Simulator(QtCore.QThread):
    movingObjArr = []
    simMain = None
    
    def __init__(self,SimMain):
        super(Simulator, self).__init__()
        #self.thread = Thread(target=self.run)
        self.daemon = True
        self.isRun = True
        self.simMain = SimMain
        
    def __del__(self):
        self.wait()
    #===========================================================================
    # Run method of the thread. In this method, moving objects created
    # and wrote down into JSON file. 
    #===========================================================================
    def run(self):
    
        self.resetAllObjectsTime()
        
        while self.isRun:
            markerList = []
            
            for i in range(len(self.movingObjArr)):
                obj = None
                obj = self.movingObjArr[i]
                elapsedTime = time() - obj.get_time()
                
                marker = {}
                marker["title"] = obj.get_id()
                # Hiz 50-110 arasinda random
                marker["hiz"] = random.randint(50,110)
                
                wayp = obj.get_waypoints()
                locIndex = obj.get_loc_index()
                
                
                # metre/sec 
                alinanYol = (obj.get_hiz()* 1000 / 3600) * int(elapsedTime)
                wpDistance = self.ikiKoordinatArasindakiMesafeBul(Coordinate(wayp[locIndex].get_latitude(),wayp[locIndex].get_longtitude()), Coordinate(wayp[locIndex + 1].get_latitude(),wayp[locIndex + 1].get_longtitude()))
                            
                #===============================================================
                # Aracin 100Km'de 10Lt yaktigini varsayarak
                # Harcadigi benzin miktarini hesaplaniyor.
                # 1 metre = 0.001 lt yakit tuketir
                #===============================================================
                anlikTuketilen = 0.0001 * alinanYol
                depodaKalanLitre = obj.get_current_fuel() - anlikTuketilen
                obj.set_current_fuel(depodaKalanLitre)
                
                if alinanYol < wpDistance and (locIndex < len(wayp)):
                    ratio = alinanYol / wpDistance
                       
                    tempLat = ratio * wayp[locIndex + 1].get_latitude() + (1 - ratio) * wayp[locIndex].get_latitude()
                    tempLng = ratio * wayp[locIndex + 1].get_longtitude() + (1 - ratio) * wayp[locIndex].get_longtitude()
                                          
                    marker["lat"] = tempLat
                    marker["long"] = tempLng
                else:
                    locIndex += 1
                    obj.set_loc_index(locIndex)
                    obj.set_time(time())
                
                marker["lat"] = wayp[locIndex].get_latitude()
                marker["long"] = wayp[locIndex].get_longtitude()
                marker["plaka"] = str(obj.get_plaka())
                marker["Yakit"] = obj.get_current_fuel()
                
                #+"- Yakit: " + str(marker['yakit'])
                marker["description"] = "No Description Available!"
                self.emit( QtCore.SIGNAL('update(QString)'), datetime.datetime.today().strftime('%d/%b/%Y %H:%M:%S  ') 
                           + "ID: "+ str(marker['title']) +" @"
                           + str(marker['lat']) + 
                           "-" + str(marker['long']) 
                           +"- Hiz: " + str(marker['hiz']) +"- Plaka: " + str(obj.get_plaka())+"- Yakit:  "+ str(obj.get_current_fuel()))
                
                markerList.append(marker)
                
            FileManager.store_file_content("../../MVT_Draft/JSONManagement/markerList.json", json.dumps(markerList))
            sleep(1)
            
        markerList = []
        FileManager.store_file_content("../../MVT_Draft/JSONManagement/markerList.json", json.dumps(markerList))
        self.terminate()
            
    def addMovingObj(self,fromLoc, destLoc):
        
        #JSON data has exact path between fromLoc and destLoc
        p = PathLoader()
        jsonData = json.loads(p.requestPath(fromLoc,destLoc))
        
        #Koordinat listesi runtime da Google API'si tarafindan olusturuldu.
        coordinateList = []
        for i in range(0,len(jsonData)):
            tempCor = Coordinate(jsonData[i]['lat'],jsonData[i]['long'])
            coordinateList.append(tempCor)
            initialFuel = round(random.uniform(38, 50), 5)
        
        
        #=======================================================================
        # id = 1
        # type = 0
        # locIndex = 0 (initially setted to 0)
        # hiz = 50
        # coordinateList = waypoints from directions service
        #=======================================================================
        tempAsset = Asset(-1,0,0,100,coordinateList, initialFuel)
        tempAsset.set_id(tempAsset.get_number_of_asset())
        self.emit( QtCore.SIGNAL('update(QString)'), datetime.datetime.today().strftime('%d/%b/%Y %H:%M:%S  ') + fromLoc +"-"+ destLoc+" arasinda varlik eklendi")
        self.movingObjArr.append(tempAsset)
        
    
    #===========================================================================
    # If simulator is stopped, then all moving objects' internal clock 
    # should be reset. 
    #===========================================================================
    def resetAllObjectsTime(self):
        for obj in self.movingObjArr:
                obj.set_time(time())
                    
    #===========================================================================
    # Finds the distance between two geocoordinate.
    #===========================================================================
    def ikiKoordinatArasindakiMesafeBul(self, ilkKonum, ikinciKonum):
        lat_1 = ilkKonum.get_latitude()*math.pi/180
        lng_1 = ilkKonum.get_longtitude()*math.pi/180
        lat_2 = ikinciKonum.get_latitude()*math.pi/180
        lng_2 = ikinciKonum.get_longtitude()*math.pi/180
        
        #Radius constant of a world
        WORLD_R = 6371
        distance = math.acos(math.sin(lat_1)*math.sin(lat_2) + (math.cos(lat_1) * math.cos(lat_2) * math.cos(lng_2-lng_1))) * WORLD_R
        
        #km to m conversion
        distance *= 1000 
        return distance
        
            
    # Stops while loop inside the thread's run method.
    def simulatorThreadDurdur(self):
        self.isRun = False