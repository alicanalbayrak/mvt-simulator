#! /usr/bin/env python
# -*- coding: UTF-8 -*-

'''
Created on Mar 15, 2014

@author: alican
'''
import sys
import PredefinedLocations
from Simulator import Simulator
from PyQt4 import QtGui, QtCore
from GUIPack.UI_SimMain import Ui_MainWindow


class Main(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.writeIntervalSpinBox.setMinimum(1)
        self.statusMessage("initial")
        self.initCmbBoxes()
        self.simButtonsInitial()
        self.isWriteLogEnabled = True;
        
        self.ui.simOynatBtn.clicked.connect(self.oynat)
        self.ui.simDuraklatBtn.clicked.connect(self.duraklat)
        self.ui.simDurdurBtn.clicked.connect(self.durdur)
        self.ui.logGosterCheckBox.stateChanged.connect(self.setLogStatus)
        self.ui.simVarlikEkle.clicked.connect(self.addMovingObjectFromGivenLocations)

        self.sim = Simulator(self)
        self.connect( self.sim, QtCore.SIGNAL("update(QString)"), self.writeLogLine )
        
        
    def simButtonsInitial(self):
        self.ui.simOynatBtn.setDisabled(False)
        self.ui.simDuraklatBtn.setDisabled(True)
        self.ui.simDurdurBtn.setDisabled(True)
        
        
    def oynat(self):
        self.ui.simOynatBtn.setDisabled(True)
        self.ui.simDuraklatBtn.setDisabled(False)
        self.ui.simDurdurBtn.setDisabled(False)
        
        self.sim.start()
        self.statusMessage("running")
        self.writeLogLine("Simulator baslatildi")
        
    
    def duraklat(self):
        self.ui.simOynatBtn.setDisabled(False)
        self.ui.simDuraklatBtn.setDisabled(True)
        self.ui.simDurdurBtn.setDisabled(False)
        
        #self.sim.isRun = False;
        self.statusMessage("paused")
        self.writeLogLine("Simulator duraklatildi.")
        
        
    def durdur(self):
        self.ui.simOynatBtn.setDisabled(False)
        self.ui.simDuraklatBtn.setDisabled(True)
        self.ui.simDurdurBtn.setDisabled(True)
        
        self.sim.simulatorThreadDurdur()
        self.statusMessage("stopped")
        self.writeLogLine("Simulator durduruldu")
        self.sim = Simulator(self)
        
        
    def statusMessage(self, stateMsg):
        self.ui.statusbar.showMessage("Simulator Status: " + stateMsg)
        
        
    def writeLogLine(self,logLine):
        if self.isWriteLogEnabled:
            self.ui.simLogBrowser.append(logLine)
            
        
    def initCmbBoxes(self):
        defCoors = PredefinedLocations.definedCoordinates
        definedPlaces = defCoors.keys()
        
        for i in range(len(definedPlaces)):
            self.ui.baslamaCmbBox.addItem(definedPlaces[i],defCoors[definedPlaces[i]]) 
            self.ui.bitisCmbBox.addItem(definedPlaces[i],defCoors[definedPlaces[i]])
        
            
    def setLogStatus(self):
        if self.ui.logGosterCheckBox.isChecked():
            self.isWriteLogEnabled = True;
        else:
            self.isWriteLogEnabled = False;
    
    
    def addMovingObjectFromGivenLocations(self):
        baslangicLoc = self.ui.baslamaCmbBox.currentText()
        bitisLoc = self.ui.bitisCmbBox.currentText()
        
        if baslangicLoc != bitisLoc:
            self.sim.addMovingObj(baslangicLoc, bitisLoc)
        else:
            QtGui.QMessageBox.warning(
                self, 'Hata', 'Lutfen, farkli noktalar secin')
        
        
def main():
    app = QtGui.QApplication(sys.argv)
    simApp = Main()
    simApp.setWindowTitle('Mobil Varlik Simulatoru - v.1.1.0')
    simApp.show()
    sys.exit(app.exec_())  

if __name__ == "__main__":
    main()