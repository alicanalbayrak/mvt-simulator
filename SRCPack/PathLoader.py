#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
Created on Mar 6, 2014

@author: alican
'''

import urllib2
import json
from Coordinate import Coordinate
from PolylineDecoder import decode_line
from PredefinedLocations import definedCoordinates

class PathLoader(object):
    

    def requestPath(self,fromLocation,toDestination):        
        a = definedCoordinates[str(fromLocation)]
        b = definedCoordinates[str(toDestination)]
        
        mapApi = "AIzaSyA-VhrjtQY9mZUI5AVKG975cQKt8DKqRWA"
        
        request_url = 'https://maps.googleapis.com/maps/api/directions/json?' +'origin=' + str(a.get_latitude()) +','+ str(a.get_longtitude()) +'&destination=' + str(b.get_latitude()) +','+ str(b.get_longtitude()) +'&sensor=false&key='+ mapApi

        requestedPathToJSON = json.load(urllib2.urlopen(request_url))
        steps = requestedPathToJSON['routes'][0]['legs'][0]['steps']
       
        waypointList = []
        for i in range (0,len(steps)):
            #print "Step: " + str(i)
            latLongArr = decode_line(steps[i]['polyline']['points'])
            
            for j in range(0,len(latLongArr)):
                waypoint = {}
                waypoint["title"] = 'no'
                waypoint["lat"] = latLongArr[j][0]
                waypoint["long"] = latLongArr[j][1]
                waypoint["description"] = "No Description Available!"
                waypointList.append(waypoint)
            
        return json.dumps(waypointList)
        #FileManager.store_file_content("../JSONManagement/markerList.json", json.dumps(markerList))

