#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
Created on Mar 1, 2014

@author: alican
'''

import random
import string
from time import time

class Asset(object):
    __NumberOfAsset = 0
    
    def __init__(self, ID, Type, locIndex, Hiz ,waypoints, initialFuel):
        self.__ID = ID
        self.__type = Type
        self.__waypoints = waypoints
        self.__locIndex = locIndex
        self.__isAssetVisible = True
        self.__Hiz = Hiz
        self.__time = time()
        self.currentFuel = initialFuel
        self.__plaka = "06 " + random.choice(string.letters).upper() + "" + random.choice(string.letters).upper() +" " + str(random.randint(111,999))
        Asset.__NumberOfAsset += 1

    def get_plaka(self):
        return self.__plaka


    def set_plaka(self, value):
        self.__plaka = value


    def get_current_fuel(self):
        return self.__currentFuel


    def set_current_fuel(self, value):
        self.__currentFuel = value


    def set_time(self, value):
        self.__time = value


    def get_time(self):
        return self.__time


    def get_id(self):
        return self.__ID


    def set_id(self, value):
        self.__ID = value
    

    def get_waypoints(self):
        return self.__waypoints


    def get_loc_index(self):
        return self.__locIndex


    def set_waypoints(self, value):
        self.__waypoints = value


    def set_loc_index(self, value):
        self.__locIndex = value


    def get_hiz(self):
        return self.__Hiz


    def set_hiz(self, value):
        self.__Hiz = value


    def get_number_of_asset(self):
        return self.__NumberOfAsset


    def get_type(self):
        return self.__type


    def get_is_asset_visible(self):
        return self.__isAssetVisible


    def set_number_of_asset(self, value):
        self.__NumberOfAsset = value


    def set_type(self, value):
        self.__type = value


    def set_is_asset_visible(self, value):
        self.__isAssetVisible = value

    NumberOfAsset = property(get_number_of_asset, set_number_of_asset, None, None)
    ID = property(get_id, None, None, None)
    type = property(get_type, set_type, None, None)
    isAssetVisible = property(get_is_asset_visible, set_is_asset_visible, None, None)
    Hiz = property(get_hiz, set_hiz, None, None)
    waypoints = property(get_waypoints, set_waypoints, None, None)
    locIndex = property(get_loc_index, set_loc_index, None, None)
    ID = property(get_id, set_id, None, None)
    time = property(get_time, None, None, None)
    time = property(None, set_time, None, None)
    currentFuel = property(get_current_fuel, set_current_fuel, None, None)
    plaka = property(get_plaka, set_plaka, None, None)
        
        
    
    
    