# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SimulatorGUIDesign.ui'
#
# Created: Mon Apr  7 17:15:52 2014
#      by: PyQt4 UI code generator 4.9.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(636, 540)
        MainWindow.setTabShape(QtGui.QTabWidget.Rounded)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.oynatimGroupBox = QtGui.QGroupBox(self.centralwidget)
        self.oynatimGroupBox.setGeometry(QtCore.QRect(10, 20, 281, 211))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.oynatimGroupBox.sizePolicy().hasHeightForWidth())
        self.oynatimGroupBox.setSizePolicy(sizePolicy)
        self.oynatimGroupBox.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.oynatimGroupBox.setAutoFillBackground(False)
        self.oynatimGroupBox.setStyleSheet(_fromUtf8("QGroupBox{\n"
"    border: 1px solid rgb(168, 174, 185);\n"
"}\n"
"\n"
"QGroupBox::title{\n"
"    background-color: transparent;\n"
"    subcontrol-position: top left;\n"
"    padding: 2 13px;\n"
"}"))
        self.oynatimGroupBox.setFlat(False)
        self.oynatimGroupBox.setCheckable(False)
        self.oynatimGroupBox.setObjectName(_fromUtf8("oynatimGroupBox"))
        self.horizontalLayoutWidget_2 = QtGui.QWidget(self.oynatimGroupBox)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(10, 120, 261, 71))
        self.horizontalLayoutWidget_2.setObjectName(_fromUtf8("horizontalLayoutWidget_2"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setMargin(0)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.simOynatBtn = QtGui.QPushButton(self.horizontalLayoutWidget_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.simOynatBtn.sizePolicy().hasHeightForWidth())
        self.simOynatBtn.setSizePolicy(sizePolicy)
        self.simOynatBtn.setStyleSheet(_fromUtf8(""))
        self.simOynatBtn.setObjectName(_fromUtf8("simOynatBtn"))
        self.horizontalLayout_2.addWidget(self.simOynatBtn)
        self.simDuraklatBtn = QtGui.QPushButton(self.horizontalLayoutWidget_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.simDuraklatBtn.sizePolicy().hasHeightForWidth())
        self.simDuraklatBtn.setSizePolicy(sizePolicy)
        self.simDuraklatBtn.setStyleSheet(_fromUtf8(""))
        self.simDuraklatBtn.setObjectName(_fromUtf8("simDuraklatBtn"))
        self.horizontalLayout_2.addWidget(self.simDuraklatBtn)
        self.simDurdurBtn = QtGui.QPushButton(self.horizontalLayoutWidget_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.simDurdurBtn.sizePolicy().hasHeightForWidth())
        self.simDurdurBtn.setSizePolicy(sizePolicy)
        self.simDurdurBtn.setObjectName(_fromUtf8("simDurdurBtn"))
        self.horizontalLayout_2.addWidget(self.simDurdurBtn)
        self.line = QtGui.QFrame(self.oynatimGroupBox)
        self.line.setGeometry(QtCore.QRect(10, 20, 261, 16))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.horizontalLayoutWidget = QtGui.QWidget(self.oynatimGroupBox)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 40, 261, 31))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.writeIntervalSpinBox = QtGui.QSpinBox(self.horizontalLayoutWidget)
        self.writeIntervalSpinBox.setObjectName(_fromUtf8("writeIntervalSpinBox"))
        self.horizontalLayout.addWidget(self.writeIntervalSpinBox)
        self.label_2 = QtGui.QLabel(self.horizontalLayoutWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout.addWidget(self.label_2)
        self.horizontalLayoutWidget_3 = QtGui.QWidget(self.oynatimGroupBox)
        self.horizontalLayoutWidget_3.setGeometry(QtCore.QRect(10, 80, 261, 31))
        self.horizontalLayoutWidget_3.setObjectName(_fromUtf8("horizontalLayoutWidget_3"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setMargin(0)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.logGosterCheckBox = QtGui.QCheckBox(self.horizontalLayoutWidget_3)
        self.logGosterCheckBox.setChecked(True)
        self.logGosterCheckBox.setObjectName(_fromUtf8("logGosterCheckBox"))
        self.horizontalLayout_3.addWidget(self.logGosterCheckBox)
        self.veriEklemeGroupBox = QtGui.QGroupBox(self.centralwidget)
        self.veriEklemeGroupBox.setGeometry(QtCore.QRect(300, 20, 321, 211))
        self.veriEklemeGroupBox.setStyleSheet(_fromUtf8("QGroupBox{\n"
"    border: 1px solid rgb(168, 174, 185);\n"
"}\n"
"\n"
"QGroupBox::title{\n"
"    background-color: transparent;\n"
"    subcontrol-position: top left;\n"
"    padding: 2 13px;\n"
"}"))
        self.veriEklemeGroupBox.setObjectName(_fromUtf8("veriEklemeGroupBox"))
        self.line_2 = QtGui.QFrame(self.veriEklemeGroupBox)
        self.line_2.setGeometry(QtCore.QRect(10, 20, 301, 16))
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.formLayoutWidget = QtGui.QWidget(self.veriEklemeGroupBox)
        self.formLayoutWidget.setGeometry(QtCore.QRect(20, 40, 281, 71))
        self.formLayoutWidget.setObjectName(_fromUtf8("formLayoutWidget"))
        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setMargin(0)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.baLamaLabel = QtGui.QLabel(self.formLayoutWidget)
        self.baLamaLabel.setObjectName(_fromUtf8("baLamaLabel"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.baLamaLabel)
        self.baslamaCmbBox = QtGui.QComboBox(self.formLayoutWidget)
        self.baslamaCmbBox.setObjectName(_fromUtf8("baslamaCmbBox"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.baslamaCmbBox)
        self.bitisLabel = QtGui.QLabel(self.formLayoutWidget)
        self.bitisLabel.setObjectName(_fromUtf8("bitisLabel"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.bitisLabel)
        self.bitisCmbBox = QtGui.QComboBox(self.formLayoutWidget)
        self.bitisCmbBox.setObjectName(_fromUtf8("bitisCmbBox"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.bitisCmbBox)
        self.simVarlikEkle = QtGui.QPushButton(self.veriEklemeGroupBox)
        self.simVarlikEkle.setGeometry(QtCore.QRect(190, 120, 111, 51))
        self.simVarlikEkle.setObjectName(_fromUtf8("simVarlikEkle"))
        self.logGroupBox = QtGui.QGroupBox(self.centralwidget)
        self.logGroupBox.setGeometry(QtCore.QRect(10, 240, 611, 271))
        self.logGroupBox.setObjectName(_fromUtf8("logGroupBox"))
        self.simLogBrowser = QtGui.QTextBrowser(self.logGroupBox)
        self.simLogBrowser.setGeometry(QtCore.QRect(0, 30, 611, 241))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.simLogBrowser.setFont(font)
        self.simLogBrowser.setObjectName(_fromUtf8("simLogBrowser"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(510, 240, 109, 19))
        self.label_3.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "Mobil Varlık Simulatörü", None, QtGui.QApplication.UnicodeUTF8))
        self.oynatimGroupBox.setTitle(QtGui.QApplication.translate("MainWindow", "Oynatım", None, QtGui.QApplication.UnicodeUTF8))
        self.simOynatBtn.setText(QtGui.QApplication.translate("MainWindow", "Oynat", None, QtGui.QApplication.UnicodeUTF8))
        self.simDuraklatBtn.setText(QtGui.QApplication.translate("MainWindow", "Duraklat", None, QtGui.QApplication.UnicodeUTF8))
        self.simDurdurBtn.setText(QtGui.QApplication.translate("MainWindow", "Durdur", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("MainWindow", "Dosya Yazma Aralığı: ", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("MainWindow", "sn", None, QtGui.QApplication.UnicodeUTF8))
        self.logGosterCheckBox.setText(QtGui.QApplication.translate("MainWindow", "Veri loglarını göster", None, QtGui.QApplication.UnicodeUTF8))
        self.veriEklemeGroupBox.setTitle(QtGui.QApplication.translate("MainWindow", "Veri Ekleme ", None, QtGui.QApplication.UnicodeUTF8))
        self.baLamaLabel.setText(QtGui.QApplication.translate("MainWindow", "Başlama: ", None, QtGui.QApplication.UnicodeUTF8))
        self.bitisLabel.setText(QtGui.QApplication.translate("MainWindow", "Bitiş: ", None, QtGui.QApplication.UnicodeUTF8))
        self.simVarlikEkle.setText(QtGui.QApplication.translate("MainWindow", "Hareket Ekle", None, QtGui.QApplication.UnicodeUTF8))
        self.logGroupBox.setTitle(QtGui.QApplication.translate("MainWindow", "Veri Logları", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("MainWindow", "00:00:00", None, QtGui.QApplication.UnicodeUTF8))

